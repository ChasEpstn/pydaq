import zmq
import threading
import struct
from qsquared import *
from tables import *
# cimport zmq
# cimport threading


cdef class TCPReader:
    cdef public object context
    cdef public object socket

    def __init__(self, addr):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.SUB)
        self.socket.connect(addr)
        self.socket.setsockopt_string(zmq.SUBSCRIBE, u"")

    cpdef void readDataStream(self, Processor):
        i = 0
        while True:
            i += 1
            # print 'a'
            line = self.socket.recv_string()
            # socket.send("")
            # print line
            lineSplit = line.split()
            ch = int(float(lineSplit[0]))
            t = float(lineSplit[1])
            # if i%100000 == 0: print i
            # print ch,t
            Processor.readStreamData(ch, t)

    cpdef void Start(self, Processor):
        # Start the daemon thread for getting new hit data
        thread = threading.Thread(target=self.readDataStream, args=(Processor,))
        thread.daemon = True
        thread.start()

class DataStorage(IsDescription):
    # channel = Float64Col()
    # time = Float64Col()
    # extendedtimetag = Float64Col()
    class GlobalHeader(IsDescription):
        EventID = Int64Col()
        GEO = Int64Col()
    class EventData(IsDescription):
        Channel = Int64Col()
        ShortTime = Int64Col()
        Trailing = Int64Col()
    class ETTT(IsDescription):
        ExtendedTime = Int64Col()
    class GlobalTrailer(IsDescription):
        Status = Int64Col()
        WordCount = Int64Col()
        GEO = Int64Col()

cdef class EventBuilder:
    cdef public long int EventID
    cdef public long int HGEO
    cdef public list Channel
    cdef public list ShortTime
    cdef public list Trailing
    cdef public long int ExtendedTime
    cdef public long int Status
    cdef public long int WordCount
    cdef public long int TGEO
    # cdef public dict CH_TIMES
    # cdef public double deadTime
    def __init__(self):
        self.EventID = 0
        self.HGEO = 0
        self.Channel = []
        self.ShortTime = []
        self.Trailing = []
        self.ExtendedTime = 0
        self.Status = 0
        self.WordCount = 0
        self.TGEO = 0
        # self.deadTime = dt
        # self.CH_TIMES = {}
        # for i in range(80):
        #     self.CH_TIMES[i] = 0.0
    # def SetETTT(self,ettt):
    #     if ettt < self.ExtendedTime:
    #         self.ExtendedTime -= 2^32
    def Clear(self):
        self.EventID = 0
        self.HGEO = 0
        self.Channel = []
        self.ShortTime = []
        self.Trailing = []
        self.ExtendedTime = 0
        self.Status = 0
        self.WordCount = 0
        self.TGEO = 0
    def AddEvent(self,ch,t,tr):
        self.Channel.append(ch)
        self.ShortTime.append(t)
        self.Trailing.append(tr)
    def GetEvents(self):
        events = zip(self.ShortTime,self.Channel,self.Trailing)
        #Need to call sort, because there's a chance the hits arrive out of order. Within an event, the shortTime should be monotonic.
        events.sort()
        events.reverse()
        return events
            # self.CH_TIMES[ch] = time
    def Sort(self):
        eve = self.GetEvents()
        self.Channel = eve[1]
        self.ShortTime = eve[0]
        self.Trailing = eve[2]

def SaveBuilder(builder,entry):
    nEve = len(builder.Channel)
    for i in range(nEve):
        entry["GlobalHeader/EventID"] = builder.EventID
        entry["GlobalHeader/GEO"] = builder.HGEO
        entry["EventData/Channel"] = builder.Channel[i]
        entry["EventData/ShortTime"] = builder.ShortTime[i]
        entry["EventData/Trailing"] = builder.Trailing[i]
        entry["ETTT/ExtendedTime"] = builder.ExtendedTime
        entry["GlobalTrailer/Status"] = builder.Status
        entry["GlobalTrailer/WordCount"] = builder.WordCount
        entry["GlobalTrailer/GEO"] = builder.TGEO
        entry.append()

def ProcessStream(hitBuffer,builder):
    for i in range(len(Events)):
        hitBuffer.AddHit(builder.Channel[i], (builder.ExtendedTime*25.0*32.0 + builder.TGEO*25.0) - builder.ShortTime[i]*0.098)

#Pass the reader a hit buffer to store hits

cdef class BufferReader:
    cdef public str fileName
    cdef public str logFileName
    cdef public str outFileName
    cdef public int seek
    cdef public long double extendedtimetag
    cdef public int on
    cdef public dict CH_TIMES
    cdef public double deadTime
    cdef public double ETTT
    # cdef public object QF
    # cdef public object Events
    def __init__(self, str fName, str lfName, str outFileName, double dt):
        self.seek = 1
        self.fileName = fName
        self.logFileName = lfName
        self.outFileName = outFileName
        self.on = 1
        self.deadTime = dt
        self.CH_TIMES = {}
        for i in range(80):
            self.CH_TIMES[i] = 0.0
        # self.QF = QFile(self.outFileName,'w')
        # self.Events = self.QF.NewTree('Events')
    cpdef void NoSeek(self):
        self.seek = 0
    cpdef void readDataStream(self, hitBuffer):
        # i = 0
        file = open(self.fileName, 'rb')
        log = open(self.logFileName, 'w')
        # cht = {}
        # extendedtimetag = 0.0
        # QF = QFile(self.outFileName,'w')
        # Events = QF.NewTree('Events')
        eBuilder = EventBuilder()
        QF = open_file(self.outFileName, mode = "w")
        DataTable = QF.create_table(QF.root, "DataTable", DataStorage)
        Entry = DataTable.row
        # Headers = QF.NewTree('Headers')
        for i in range(100):
            file.seek(0)
            try:
                file.read(4096)
            except(IOError):
                break

        while self.on == 1:
            if self.seek == 1:
                file.seek(0)
            try:
                line = file.read(4096)
            except(IOError):
                continue
            # for i in range(len(line)):
            #     print line[i]>>27
            # print len(line)
            #extendedtimetag = 0
            for i in range(len(line) / 4):
                k = i * 4
                word = line[(k):(k + 4)]
                word = struct.unpack('I', word)[0]
                headertype = word >> 27
                if headertype == 0: # EVENT
                    channel = (word >> 19) & 0x7f
                    time = word & 0x7FFFF #0x3FFFF before
                    trailing = (word >> 26) & 0x1
                    # cht['ch'] = channel
                    # print headertype
                    # print channel,time
                    # Processor.readStreamData(channel, time)
                    # Processor.readStreamData(channel, extendedtimetag*25.0*32.0 - time*0.098)
                    # Events.Add((channel,time,extendedtimetag))
                    # event['channel'] = channel
                    # event['time'] = time
                    # event['extendedtimetag'] = extendedtimetag
                    # event.append()
                    # t0 = eBuilder.ExtendedTime*25.0*32.0 + eBuilder.TGEO*25.0 + time*0.098
                    # # print eBuilder.ExtendedTime,eBuilder.TGEO,time
                    # if (t0-self.CH_TIMES[channel] < 0):
                    #     t = t0 + (2^32)*25.0*32.0
                    # else:
                    #     t = t0
                    # if (t-self.CH_TIMES[channel] > self.deadTime):
                    if channel not in range(0,80):
                        continue
                    if t*0.098 > 50000:
                        continue
                    eBuilder.AddEvent(channel,time,trailing)
                        # self.CH_TIMES[channel] = t0
                    # log.write("Event info: Channel: {} Time: {}, time tag: {}\n".format(channel,time,extendedtimetag*25.0*32.0 - time*0.098))
                elif headertype == 17: # ETTT
                    # extendedtimetag = word & 0x3ffffff
                    extendedtimetag = word & 0x07ffffff
                    #cht['t'] = extendedtimetag
                    eBuilder.ExtendedTime = extendedtimetag
                    # log.write("Extended Time Tag {}\n".format(extendedtimetag))
                elif headertype == 1:  # TDC header
                    tdcnr = (word >> 24) & 0x3
                    eventid = (word >> 12) & 0xfff
                    bunchid = word & 0xfff
                    log.write("  TDC header, TDC {}, event {}, bunch {}\n".format(
                        tdcnr, eventid, bunchid))
                elif headertype == 3:  # TDC trailer
                    tdcnr = (word >> 24) & 0x3
                    eventid = (word >> 12) & 0xfff
                    wordcount = word & 0xfff
                    log.write("  TDC trailer, TDC {}, event {}, words {}\n".format(
                        tdcnr, eventid, wordcount))
                elif headertype == 4:  # tdc error
                    tdcnr = (word >> 24) & 0x3
                    errorflag = word & 0x3fff
                    # tdc->errors.push_back(word);
                    errorMSG = "  TDC error, TDC {}, flags {}\n".format(
                        tdcnr, errorflag)
#                    with self.term.location(0, 25):
#                        print errorMSG
                    log.write(errorMSG)
                elif headertype == 8:  # global header
                    eventid = (word >> 5) & 0x3fffff
                    geo = word & 0x1f
                    # log.write(
                    #     "Global header, event {}, geo {}\n".format(eventid, geo))
                    eBuilder.EventID = eventid
                    eBuilder.HGEO = geo
                elif headertype == 16: # global trailer
                    status = (word >> 24) & 0x7
                    wordcount = (word >> 5) & 0xffff
                    geo = word & 0x1f
                    eBuilder.Status = status
                    eBuilder.WordCount = wordcount
                    eBuilder.TGEO = geo

                    eBuilder.Sort()
                    SaveBuilder(eBuilder,Entry)

                    newCH = []
                    newST = []
                    newTR = []

                    #Record the current bunch clock
                    self.ETTT = eBuilder.ExtendedTime*25.0*32.0 + eBuilder.TGEO*25.0

                    for i in range(len(eBuilder.Channel)):
                        #Calculate the abs time of the hit
                        t0 = self.ETTT - eBuilder.ShortTime[i]*0.098
                        # print eBuilder.ExtendedTime,eBuilder.TGEO,time

                        #Get the channel
                        ch = eBuilder.Channel[i]

                        #If the current time is less than the existing time,
                        #then the ETTT has rolled over. So, subtract it from CH_TIMES
                        if (t0-self.CH_TIMES[ch] < 0):
                            for ch in self.CH_TIMES.keys():
                                self.CH_TIMES[ch] -= float(2**32)*25.0
                        #     t = t0 + float(2**32)*25.0*32.0
                        # else:
                        #     t = t0
                        t = t0
                        #if the current time is > deadtime away from the last time
                        if (t-self.CH_TIMES[ch] > self.deadTime):
                            # eBuilder.AddEvent(channel,time,trailing)
                            #Save that event
                            newCH.append(eBuilder.Channel[i])
                            newST.append(eBuilder.ShortTime[i])
                            newTR.append(eBuilder.Trailing[i])
                            #Record that time in the db
                            self.CH_TIMES[channel] = t0
                    eBuilder.Channel = newCH
                    eBuilder.ShortTime = newST
                    eBuilder.Trailing = newTR

                    ProcessStream(hitBuffer,eBuilder)
                    eBuilder.Clear()
                    #log.write("Global trailer, status {}, wordcount {}, geo {}\n".format(
                    #     status, wordcount, geo))
                else:
                    log.write("Could not decode {} {}\n".format(
                        headertype, word))
                # if len(cht.keys()) == 1:
                #    Processor.readStreamData(cht['ch'], extendedtimetag)
                #    cht = {}
            #log.write('Looping\n')

        DataTable.flush()
        QF.close()
        #log.write('Done\n')
    cpdef void Start(self, hitBuffer):
        # Start the daemon thread for getting new hit data
        thread = threading.Thread(target=self.readDataStream, args=(hitBuffer,))
        # thread.daemon = True
        thread.start()
    cpdef void Stop(self):
        #log.write('Calling stop function\n')
        # print 'Calling Stop Function'
        self.on = 0
        # print 'ON: {}'.format(self.on)
