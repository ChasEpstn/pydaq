import plotly
# import numpy as np
import plotly.plotly as py
import plotly.tools as tls
import plotly.graph_objs as go

stream_ids = tls.get_credentials_file()['stream_ids']

# Get stream id from stream id list
stream_id = stream_ids[0]

# Make instance of stream id object
stream_1 = go.Stream(
    token=stream_id,  # link stream id to 'token' key
)

HeatMap1 = go.Heatmap(
    colorscale='Viridis',
    stream=stream_1         # (!) embed stream id, 1 per trace
)

data = go.Data([HeatMap1])

# Add title to layout object
layout = go.Layout(
    title='Detector Hits',
    width=700,
    height=350
)

# Make a figure object
fig = go.Figure(data=data, layout=layout)

# Send fig to Plotly, initialize streaming plot, open new tab
py.iplot(fig, filename='DetectorMap')

# We will provide the stream link object the same token that's associated with the trace we wish to stream to
s = py.Stream(stream_id)
