from __future__ import division
import numpy as np
cimport numpy as np
from colored import bg, attr


cpdef void line(np.ndarray cols, term, int x, int y):
    l = len(cols)
    space = '  '
    s = ('%s' + space + str(attr('reset'))) * l
    try:
        a = tuple(map(bg, cols))
    except(AttributeError):
        return
    with term.location(x, y):
        print(s % a)

cpdef void print_array(np.ndarray arr, term, int x, int y0):
    arrMax = arr.max()
    if arrMax > 0:
        arr = (232 + (255 - 232) * (arr / arrMax)).astype(int)
    else:
        arr = arr.astype(int)
        arr[:,:] = 232
    y = y0
    for lineProc in arr:
        # print lineProc
        line(lineProc, term, x, y)
        y += 1

# if __name__ == "__main__":
#     a = np.array([range(10), range(10)[::-1]])
#     print_array(a)

# line([250,240])
# line([240,250])

# print c[5]
