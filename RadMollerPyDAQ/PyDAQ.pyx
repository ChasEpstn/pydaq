from __future__ import division
import time
# import Coincidence as c
import threading
import zmq
import numpy as np
cimport numpy as np
import Readers
import os
import sys
from Display import *
from Processors import *
from Detectors import *

cdef class PyDAQ:
    cdef public object Reader
    cdef public object Processor
    cdef public object Detector
    cdef public object Buffer
    cdef public int PLOTLY
    cdef public int ASCII
    cdef public object term
    cdef public int on
    def __init__(self,Reader,Processor,Detector):
        from blessings import Terminal
        self.term = Terminal()
        self.on = 1
        self.Reader = Reader
        self.Processor = Processor
        self.Detector = Detector
        self.Buffer = HitBuffer()
    # cpdef void StartTCPReader(self, addr):
    #     self.Reader = Readers.TCPReader(addr)
    #     self.Reader.Start(self.Processor)
    # cpdef void StartBufferReader(self, path, log, qf, deadTime):
    #     self.Reader = Readers.BufferReader(path, log, qf, deadTime)
    #     self.Reader.Start(self.Processor)
    # # cpdef void StopBufferReader(self):
    # #     self.Reader.Stop()
    # cpdef void StartBufferReaderNoSeek(self, path, log, qf, deadTime):
    #     self.Reader = Readers.BufferReader(path, log, qf, deadTime)
    #     self.Reader.NoSeek()
    #     self.Reader.Start(self.Processor)
    cpdef void StartReader(self):
        self.Reader.Start(self.Buffer)
    cpdef void PlotToPlotly(self):
        self.PLOTLY = True
        import streamToPlotly as stream
        # We then open a connection to Plotly
        stream.s.open()
    cpdef void PlotAsASCII(self):
        self.ASCII = True
        os.system('cls' if os.name == 'nt' else 'clear')
    cpdef void StartProcessor(self):
        self.Processor.Start(self.Buffer)
    cpdef void StartDetector(self, float accTime):
        if isinstance(self.Detector,ChannelRates):
            os.system('cls' if os.name == 'nt' else 'clear')
            for i in range(40):
                with self.term.location(0, i+1):
                    print "Channel {}: ".format(i)
            for i in range(40,80):
                with self.term.location(30, i-40+1):
                    print "Channel {}: ".format(i)
        try:
            while self.on==1:
                # Pull off recorded hits
                startTime = time.time()
                chPairs = self.Processor.getHits()
                # Process them and get a coincidence map
                locs = self.Detector.processHits(chPairs)
                if isinstance(self.Detector,ChannelRates):
                    for i in range(40):
                        with self.term.location(0+15, i):
                            print str(int(locs[i,1]))+' '*5
                    for i in range(40,80):
                        with self.term.location(30+15, i-40):
                            print str(int(locs[i,1]))+' '*5
                elif self.PLOTLY:
                    stream.s.write(dict(z=locs[0], type='heatmap'))
                elif self.ASCII:
                    toplayer = self.Detector.TopLayer
                    bottomlayer = self.Detector.BottomLayer
                    with self.term.location(0, 35):
                        print "Read {} Total Hits".format(self.Processor.tothits)
                    with self.term.location(0,0):
                        print "Total Rate: {}".format(np.sum(locs[1])-3160)
                    # with self.term.location(4, 4):
                    off_x = 10
                    off_y = 4
                    print_array(locs[0],self.term,off_x,off_y)
                    for chCount in locs[1]:
                        ch,count = chCount
                        if ch in toplayer:
                            x = 0
                            y = toplayer[ch] + off_y
                            with self.term.location(x,y):
                                print str(int(count))+' '*5
                        elif ch in bottomlayer:
                            x = bottomlayer[ch]*2 + off_x
                            y = off_y + 16 + 2
                            for c in str(int(count))+'     ':
                                with self.term.location(x,y):
                                    print c
                                y += 1
                        else:
                            pass
                else:
                    with self.term.location(0,37):
                        print "Total Rate: {}".format(np.sum(locs[1]))
                    #print locs
                endTime = time.time()
                sleepTime = accTime-(endTime-startTime)
                time.sleep(sleepTime if sleepTime>0 else 0)
        except KeyboardInterrupt:
            os.system('cls' if os.name == 'nt' else 'clear')
            # sys.exit()
    cpdef void Stop(self):
        self.Reader.Stop()
        self.Processor.Stop()
        self.on = 0


cdef class HitBuffer:
    cdef public list bufferedHits
    def __init__(self):
        self.bufferedHits = []
    cpdef void setup(self):
        self.bufferedHits = []
    cpdef list getRawHits(self):
        size = len(self.bufferedHits)
        newHits = self.bufferedHits[0:size]
        del self.bufferedHits[0:size]
        return newHits
    cpdef void AddHit(self,ch,time):
        self.bufferedHits.append((ch,time))
