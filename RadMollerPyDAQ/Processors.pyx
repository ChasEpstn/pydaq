import numpy as np
cimport numpy as np
import itertools
import os
import threading
# import ast



#Pass the processor a buffer from which to pull hits

cdef class DirectProcessor:
    cdef public list hits
    cdef public int tothits
    cdef public int on
    def __init__(self):
        self.hits = []
        self.tothits = 0
    cpdef void setup(self):
        self.hits = []
    cpdef void readStreamData(self, int channel, double hitTime):
        self.tothits = self.tothits + 1
        self.hits.append(channel)
    # Run through a list of (hit,time) pairs
    cpdef void processStream(self, buffer):
        while self.on == 1:
            for ch, t in buffer.getRawHits():
                self.readStreamData(ch, t)
        # Reset when done
        # self.setup()
    cpdef list getHits(self):
        size = len(self.hits)
        newHits = self.hits[0:size]
        del self.hits[0:size]
        return newHits
    cpdef void Start(self, buffer):
        # Start the daemon thread for getting new hit data
        thread = threading.Thread(target=self.processStream, args=(buffer,))
        # thread.daemon = True
        thread.start()
    cpdef void Stop(self):
        #log.write('Calling stop function\n')
        # print 'Calling Stop Function'
        self.on = 0

cdef class CoincidenceProcessor:
    cdef public double CoincidenceWindow
    cdef public list coincidenceHits
    cdef public list hits
    cdef public double time
    cdef public int tothits
    cdef public int on
    def __init__(self, double CoincidenceWindow):
        self.setup(CoincidenceWindow)
        self.coincidenceHits = []
        self.tothits = 0
    cdef void setup(self, double CoincidenceWindow):
        self.CoincidenceWindow = CoincidenceWindow
        self.hits = []
        self.time = -1
    cpdef void readStreamData(self, int channel, double hitTime):
        self.tothits = self.tothits + 1
        #print channel, hitTime

        # If the new hit falls in the window
        if self.time == -1:
            self.time = hitTime
        elif hitTime < self.time:
            self.time = hitTime
        elif (hitTime - self.time) < self.CoincidenceWindow:
            self.hits.append([channel,hitTime])
        # if not
        else:
            # if we had collected more than one hit, it's a coincidence
            if len(self.hits) > 1:
                self.time = hitTime
                self.coincidenceHits.append(self.hits)
                # print self.hits
                self.hits = [[channel,hitTime]]

            # if the last hit had no coincidence, reset
            else:
                self.time = hitTime
                self.hits = [[channel,hitTime]]

    # Run through a list of (hit,time) pairs
    cpdef void processStream(self, buffer):
        while self.on == 1:
            for ch, t in buffer.getRawHits():
                self.readStreamData(ch, t)
        # Reset when done
        # self.setup(self.CoincidenceWindow)

    cpdef list getHits(self):
        size = len(self.coincidenceHits)
        newHits = self.coincidenceHits[0:size]
        del self.coincidenceHits[0:size]
        return newHits
    cpdef void Start(self, buffer):
        # Start the daemon thread for getting new hit data
        thread = threading.Thread(target=self.processStream, args=(buffer,))
        # thread.daemon = True
        thread.start()
    cpdef void Stop(self):
        #log.write('Calling stop function\n')
        # print 'Calling Stop Function'
        self.on = 0
