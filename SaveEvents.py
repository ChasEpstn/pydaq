from qsquared import *
import numpy as np
import time

nPars = 10
nEve = 10000
fileName = 'Test1.qs'

QF = QFile(fileName,'w')
QT = QF.NewTree('Events')

startTime = time.time()

for i in range(nEve):
    QT.Add((1,2,3,4))#np.random.random(nPars))
    # QF.db.pack()
    # transaction.commit()
print "Done"
QF.Close()
stopTime = time.time()
print '{} events with {} parameters, in {} seconds'.format(nEve,nPars,(stopTime-startTime))
