import struct


fileName = 'dump.dat'
file = open(fileName,'r')
#
# file.seek(0,0)
#
while True:
    line = file.read(4096)
    # for i in range(len(line)):
    #     print line[i]>>27
    # print len(line)
    for i in range(len(line)/4):
        k = i*4
        word = line[(k):(k+4)]
        word = struct.unpack('I', word)[0]
        headertype = word>>27
        if headertype == 0:
            channel=(word >>19 )&0x7f
            time=word & 0x3FFFF
            trailing=(word>>26)&0x1
            # print headertype
            # print channel,time,trailing
    	elif headertype == 17:
            extendedtimetag=word & 0x3ffffff
            print "Extended Time Tag {}\n".format(extendedtimetag)
        elif headertype == 1: #TDC header
            tdcnr=(word >>24) & 0x3;
            eventid=(word >>12)&  0xfff;
            bunchid=word & 0xfff;
            print "  TDC header, TDC {}, event {}, bunch {}\n".format(tdcnr,eventid,bunchid)
        elif headertype == 3: #TDC trailer
            tdcnr=(word >>24) & 0x3;
            eventid=(word >>12)&  0xfff;
            wordcount=word & 0xfff;
            print "  TDC trailer, TDC {}, event {}, words {}\n".format(tdcnr,eventid,wordcount)
        elif headertype == 4: #tdc error
            tdcnr=(word >>24) & 0x3;
            errorflag=word &0x3fff;
            # tdc->errors.push_back(word);
            print "  TDC error, TDC {}, flags {}\n".format(tdcnr,errorflag)
        elif headertype == 8: #global header
            eventid=(word>>5)&0x3fffff;
            geo= word & 0x1f;
            print "Global header, event {}, geo {}\n".format(eventid,geo)
        elif headertype == 16:
            status=(word>>24) &0x7;
            wordcount=(word>>5) & 0xffff;
            geo= word & 0x1f;
            print "Global trailer, status {}, wordcount {}, geo {}\n".format(status, wordcount,geo)
        else:
            print "Could not decode {} {}\n".format(headertype,word)

        # print time
    # print bin(word)

# print line
# for line in file.readlines():
#     #       unsigned int word=buf[i];
#     #   unsigned int headertype=word>>27;
#     print line<<27
