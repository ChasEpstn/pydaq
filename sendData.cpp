//
//  Hello World client in C++
//  Connects REQ socket to tcp://localhost:5555
//  Sends "Hello" to server, expects "World" back
//
#include <zmq.hpp>
#include <string>
#include <iostream>
#include <random>
#include <string>
#include <sstream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


int main ()
{
    //  Prepare our context and socket
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_PUB);
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> tl(0, 15);
    std::uniform_int_distribution<> bl(0, 59);
    std::ostringstream oss;

    double rate = 1000000.0;
    double dt = 0.001;
    int TopLayer[16] = {0, 48, 1, 47, 2, 46, 3, 45, 4, 44, 5, 43, 6, 42, 7, 41};
    int BottomLayer[60] = {9, 79, 10, 78, 11, 77, 12, 76, 13, 75, 14,
        74, 15, 73, 16, 72, 17, 71, 18, 70, 19, 69, 20, 68, 21, 67, 22, 66,
         23, 65, 24, 64, 25, 63, 26, 62, 27, 61, 28, 60, 29, 59, 30, 58,
          31, 57, 32, 56, 33, 55, 34, 54, 35, 53, 36, 52, 37, 51, 38, 50};

    std::cout << "Connecting to data server" << std::endl;
    socket.bind ("tcp://*:5556");

    // std::string s = "";
    //  Do 10 requests, waiting each time for a response

    // for (int timer = 1; timer < 1000000; timer++) {
    int timer = 0;
    while(true){
        timer++;
        if(timer%100000 == 0) printf("%d \n",timer);

        //Create two messages, each 25 characters long.
        //These will hold the (channel, time) pairs, represented as a string
        zmq::message_t message1 (25); //Top layer
        zmq::message_t message2 (25); //Bottom layer

        // printf("New loop %d\n",timer);

        //Pick the channels and times
        int top = TopLayer[tl(gen)];
        int bot = BottomLayer[bl(gen)];
        double t0 = timer;
        double t1 = timer + dt;
        // oss << top << " " << t0;
        // s = std::to_string(top) + " " + std::to_string(t0);


        snprintf ((char*) message1.data(), 25, "%d %.5f ",top, t0);
        // printf("printed 1 \n");
        // memcpy (request.data (), s.c_str(), 25);
        // std::cout << "Sending Hello " << request_nbr << "…" << std::endl;
        socket.send (message1);
        // usleep(1e3);

        // s = std::to_string(bot) + " " + std::to_string(t1);
        snprintf ((char*) message2.data(), 25, "%d %.5f ",bot, t1);
        // printf("printed 2 \n");

        // oss << bot << " " << t1;
        // zmq::message_t request (25);
        // memcpy (request.data (), s.c_str(), 25);
        // std::cout << "Sending Hello " << request_nbr << "…" << std::endl;
        socket.send (message2);
        // printf("sent \n");

        usleep((int) 1e6/rate);
        // usleep((int) 1e6/rate);
        //  Get the reply.
        // zmq::message_t reply;
        // socket.recv (&reply);
        // std::cout << "Received World " << request_nbr << std::endl;
    }
    return 0;
}
