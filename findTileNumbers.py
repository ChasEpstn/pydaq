#!/usr/bin/env python

file = open('TileMap.dat','w')

nPerBoard = 8

# endBoards = [[1,6]]
endBoards = [[1,6]]

# sides = [2,3,4,5,7,8,9,10]
sideBoardPairs = [[2,10],[3,9],[4,8],[5,7]]

endLayer = []

def makeLayer(boards):
    layer = []
    for pair in boards:
        for i in range(nPerBoard):#[::-1]:
            layer.append((nPerBoard-i-1) + nPerBoard*(pair[1]-1))
            layer.append(i + nPerBoard*(pair[0]-1))
    return layer

EB = makeLayer(endBoards)[::-1]

SB = makeLayer(sideBoardPairs)[2:-2]

file.write("{}\n".format(EB))
file.write("{}\n".format(SB))
