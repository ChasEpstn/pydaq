#/bin/bash

modprobe vme_ca91cx42 geoid=1
modprobe vme_vmivme7805


cd /usr/src/vmedrivers/v1190
make -j && insmod v1190.ko addr=0x1111
cd ..

#matchwindowwidth windowoffset extrasearchwindowwidth rejectmargin trigtimesubenabled
# 0x14 0xffd8  0x8 0x4 0x00
#echo 0x100  0xff00 0x8 0x4 0x00 > /sys/bus/vme/devices/v1190.0-0/trigger
#echo 0xff0 0xf801  0x08 0x01 0x00 > /sys/bus/vme/devices/v1190.0-0/trigger
#echo 0x7f0 0xf80f  0x08 0x01 0x01 > /sys/bus/vme/devices/v1190.0-0/trigger


#old one. Note: 0x7f0 = 2032*25ns = 50.8 us.
# echo 0x7f0 0xf80f  0x08 0x01 0x01 > /sys/bus/vme/devices/v1190.0-0/trigger

#new one. Note 0x800=2048 = 51.2 us. 0xF800 = -2048. Trigger subtraction _disabled_!.
#Also, 0xF810 = -2032
#Moving reject margin up to 2. This just holds onto hits a little longer before they're discarded (if no trigger received)
echo 0x7F0 0xF810 0x08 0x02 0x00 > /sys/bus/vme/devices/v1190.0-0/trigger
sleep 1

#This sets bits 5, 9, and 11 to the control register.
#Bit 5: integral nonlinearity compensation. Bits  9&11: ETTT
echo 0xa20 > /sys/bus/vme/devices/v1190.0-0/ctrl
sleep 1

#0xa20 = 12544
#I think this disables the TDC Header and Trailer during readout
echo 0x3100 > /sys/bus/vme/devices/v1190.0-0/uC
sleep 1

#0x3300 = 13056. 0x0009 = 9. I think this sets the max number of hits per event.
#0x0009 means 1001 = unlimited. 
echo 0x3300 > /sys/bus/vme/devices/v1190.0-0/uC
echo 0x0009 > /sys/bus/vme/devices/v1190.0-0/uC
# echo 0x0017 > /sys/bus/vme/devices/v1190.0-0/uC
sleep 1

#1 sets TMM. 0 sets continuous storage mode
echo 1 > /sys/bus/vme/devices/v1190.0-0/storage_mode
