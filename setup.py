
from setuptools import setup, Extension
from Cython.Build import cythonize
import numpy as np

extensions = [Extension("RadMollerPyDAQ.Processors", ["RadMollerPyDAQ/Processors.pyx"],
                        include_dirs=[np.get_include()])]#,extra_compile_args=["-O3"])]
extensions.append(Extension("RadMollerPyDAQ.Readers", [
                  "RadMollerPyDAQ/Readers.pyx"] ))# extra_compile_args=["-O3"]))
extensions.append(
    Extension("RadMollerPyDAQ.Display", ["RadMollerPyDAQ/Display.pyx"], include_dirs=[np.get_include()]))#,extra_compile_args=["-O3"]))
extensions.append(
    Extension("RadMollerPyDAQ.PyDAQ", ["RadMollerPyDAQ/PyDAQ.pyx"], include_dirs=[np.get_include()]))#,extra_compile_args=["-O3"]))
extensions.append(
    Extension("RadMollerPyDAQ.Detectors", ["RadMollerPyDAQ/Detectors.pyx"], include_dirs=[np.get_include()]))#,extra_compile_args=["-O3"]))


setup(
    name='RadMollerPyDAQ',
    packages=['RadMollerPyDAQ'],
    version='0.1',
    install_requires=[
        "numpy",
        "Cython",
        "blessings",
        "zmq",
        "plotly",
        "colored",
        "tables"
    ],
    package_data = {
        'RadMollerPyDAQ' : ['TileMap.dat'],
    },
    author="Charles Epstein",
    author_email="cepstein@mit.edu",
    description="Radiative Moller Python DAQ",
    ext_modules=cythonize(extensions),

)


# run:
# python setup.py build_ext --inplace --force
