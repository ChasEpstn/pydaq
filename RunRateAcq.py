

from __future__ import division
import time
from RadMollerPyDAQ import *
import numpy as np
import sys, os
import argparse
import signal
import ArduinoControl as AC

ACC_TIME = 1
BUFFER_PATH = "/sys/bus/vme/devices/v1190.0-0/data"
OUTPUT_FILE = "dump.pt"
DEADTIME = 1000

Channel_List = range(8,16)#[10,15]

Threshold_Min = 170 
Threshold_Max = 210
Threshold_Step = 1

Threshold_Steps = np.arange(Threshold_Min,Threshold_Max+1,Threshold_Step)



DAQ = PyDAQ()
Processor = DirectReader()
Detector = ChannelRates()

Arduino = AC.ArduinoController()
Arduino.SetNBoards(10)
time.sleep(1)

def signal_handler(*args):
        # print('You pressed Ctrl+C!')
        DAQ.StopBufferReader()
        DAQ.Stop()
        os.system('cls' if os.name == 'nt' else 'clear')
        sys.exit()

signal.signal(signal.SIGINT, signal_handler)


DAQ.StartBufferReader(Processor, BUFFER_PATH, "logfile1.txt",OUTPUT_FILE,DEADTIME)

def GetHitRates(detector, processor, accTime, channels):
    hitRates = np.zeros(80)
    processor.setup()
    time.sleep(accTime)
    chPairs = processor.getHits()
    # Process them and get a coincidence map
    locs = detector.processHits(chPairs)
    hitRates += locs[:,1]
    selectedRates = hitRates[channels]
    #print selectedRates
    return selectedRates


timestr = time.strftime("%Y%m%d-%H%M%S")
chs = ''
for ch in Channel_List:
    chs += str(ch)+'_'

FILENAME = 'Channels_{}Thresh_{}_To_{}_{}.txt'.format(chs,Threshold_Min,Threshold_Max,timestr)

f = open(FILENAME,'w')
f.write('Channel\t Threshold\t Number in {}s\n'.format(ACC_TIME))

#for channel in Channel_List:
#    Arduino.SetAll(4000)
#    time.sleep(1)
#    for thresh in Threshold_Steps:
#        print 'Channel: {}, Threshold: {}'.format(channel,thresh)
#        Arduino.SetTDCchannel(channel,thresh)
#        time.sleep(1)
#        rate = GetHitRates(Detector,Processor,ACC_TIME,channel)
#        f.write("{}\t{}\t{}\n".format(channel,thresh,rate))
#        print '\t',rate

for thresh in Threshold_Steps:
    #print 'Channel: {}, Threshold: {}'.format(channel,thresh)
    print 'Threshold: {}'.format(thresh)
    for channel in Channel_List:
        Arduino.SetTDCchannel(channel,thresh)
        time.sleep(0.1)
    rates = GetHitRates(Detector,Processor,ACC_TIME,Channel_List)
    for i in range(len(Channel_List)):
        f.write("{}\t{}\t{}\n".format(Channel_List[i],thresh,rates[i]))
        print '\t',rates[i]
    os.system('rm dump.pt')

DAQ.StopBufferReader()
DAQ.Stop()
sys.exit()
