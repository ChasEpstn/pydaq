from __future__ import division
import time
from RadMollerPyDAQ import *
import numpy as np
import sys, os
import argparse
import signal

MODES = ['CHANNEL_RATE', 'DIRECT', 'COINCIDENCE','RATE']

MODE = MODES[0]

HISTORY = None

COINCIDENCE_WINDOW = 30000

ACC_TIME = 0.5

BUFFER_PATH = "/sys/bus/vme/devices/v1190.0-0/data"

parser = argparse.ArgumentParser()

parser.add_argument('-r', '--rates', action='store_const', dest='mode_val',
                    const='CHANNEL_RATE', help='Show individual channel rates.')
parser.add_argument('-d', '--direct', action='store_const', dest='mode_val',
                    const='DIRECT', help='Show running plot of strip rates.')
parser.add_argument('-c', '--coincidence', action='store_const', dest='mode_val',
                    const='COINCIDENCE', help='Show running plot of coincidences')

parser.add_argument('-w', '--window', action='store', dest='coinc_window',
                    help='Set the coincidence window, in ns.')

parser.add_argument('-a', '--accumulate', action='store', dest='acc_time',
                    help='Set the accumulation time, in s.')

parser.add_argument('-b', '--bufferpath', action='store', dest='buff_path',
                    help='Set the buffer path.')

parser.add_argument('-f', '--history', action='store', dest='histFrac',
                    help='Set the fraction of the Direct plot retained on next frame.')

parser.add_argument('-t', '--tcp', action='store_true', dest='tcp',
                    help='Read from ZMQ tcp stream.')

parser.add_argument('-s', '--dump', action='store_true', dest='dump',
                    help='Dump hit map to screen.')

parser.add_argument('-o', '--output', action='store', dest='output_file',
                    help='Output QS file name.')

parser.add_argument('-x', '--suppress', action='store', dest='suppress_time',
                    help='Artificial deadtime for online display (In ns. Default 1us)')

OUTPUT_FILE = "output.pt"
DEADTIME = 1000.0 #ns
results = parser.parse_args()
if results.mode_val:
    MODE = results.mode_val
if results.coinc_window:
    COINCIDENCE_WINDOW = float(results.coinc_window)
if results.acc_time:
    ACC_TIME = float(results.acc_time)
if results.buff_path:
    BUFFER_PATH = results.buff_path
if results.histFrac:
    HISTORY = float(results.histFrac)
if results.output_file:
    OUTPUT_FILE = results.output_file
if results.suppress_time:
    DEADTIME = float(results.suppress_time)

print MODE
print COINCIDENCE_WINDOW
print ACC_TIME
print BUFFER_PATH
# sys.exit()



if MODE == MODES[0]:  # Channel rate printout
    Processor = DirectProcessor()
    Detector = ChannelRates()
elif MODE == MODES[1]:  # Direct
    Processor = DirectProcessor()
    Detector = DirectTileArray()
    if HISTORY:
        Detector.SetHistory(HISTORY)
elif MODE == MODES[2]:  # Coincidence
    Processor = CoincidenceProcessor(COINCIDENCE_WINDOW)
    Detector = CoincidenceTileArray()
    if HISTORY:
        Detector.SetHistory(HISTORY)

else:
    print 'Unknown mode! Exiting.'
    sys.exit()


if results.tcp:
    Reader = TCPReader("tcp://localhost:5556")
else:
    Reader = BufferReader("dump.dat", "logfile1.txt", OUTPUT_FILE,DEADTIME)
    Reader.NoSeek()
    # Reader = BufferReader(BUFFER_PATH, "logfile1.txt",OUTPUT_FILE,DEADTIME)

DAQ = PyDAQ(Reader, Processor, Detector)

if not results.dump:
    DAQ.PlotAsASCII()

def signal_handler(*args):
        # print('You pressed Ctrl+C!')
        # DAQ.StopBufferReader()
        DAQ.Stop()
        os.system('cls' if os.name == 'nt' else 'clear')
        sys.exit()

signal.signal(signal.SIGINT, signal_handler)

#
        # DAQ.PD.on=0
DAQ.StartReader()
DAQ.StartProcessor()
DAQ.StartDetector(ACC_TIME)
