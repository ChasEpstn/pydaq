import serial
import io
import time
from datetime import datetime
import threading
from numbers import Number
from math import *
# serial_port = serial.Serial(port, baud, timeout=0)
#
# def handle_data(data):
#     print(data)
#
# def read_from_port(ser):
#     while not connected:
#         #serin = ser.read()
#         connected = True
#
#         while True:
#            print("test")
#            reading = ser.readline().decode()
#            handle_data(reading)
#
# thread = threading.Thread(target=read_from_port, args=(serial_port,))
# thread.start()

class ChannelConverter:
    def __init__(self):
        pass
    def TDCtoArduino(self,ch):
        board = int(floor(ch/8.0))
        aCh = 7 - (ch % 8)
        return board,aCh
    def ArduinoToTDC(self,board,ch):
        return board*8 + (7-ch)


class ArduinoController:
    def __init__(self,addr='/dev/arduino',br=115200,tout=0.5):
        self.addr = addr
        self.br = br
        self.tout = tout
        self.isopen = False
        self.ser = self.OpenConnection()
        self.conv = ChannelConverter()
        # self.sio = self.MakeIO()
        self.lock = False
        # self.eve = threading.Event()
        # self.eve.set()
    def OpenConnection(self):
        s = serial.Serial(self.addr,self.br,timeout=self.tout)
        self.isopen = True
        return s
    # def MakeIO(self):
    #     assert self.isopen
    #     sio = io.TextIOWrapper(io.BufferedRWPair(self.ser, self.ser))
    #     return sio
    def Write(self,cmd):
        cmd = cmd + "\n\r"
        self.ser.write(cmd)
        rb = self.ser.readline()
        return rb
    # def WriteIO(self,cmd):
    #     cmd = cmd + "\n\r"
    #     self.sio.write(unicode(cmd))
    #     self.sio.flush()
    #     rb = self.sio.readline()
    #     return rb
    def SetNBoards(self,nboards):
        self.Write("n {}".format(nboards))
    def SetAll(self,thresh):
        self.Write("a {}".format(thresh))
    def SetBoard(self,board,thresh):
        self.Write("b {} {}".format(board,thresh))
    def SetChannel(self,board,channel,thresh):
        self.Write("c {} {} {}".format(board,channel,thresh))
    def SetTDCchannel(self,TDCchannel,thresh):
        board,channel = self.conv.TDCtoArduino(TDCchannel)
        self.Write("c {} {} {}".format(board,channel,thresh))
    def Reprogram(self):
        self.Write("r")
    def Push(self):
        self.Write("p")
