import numpy as np
import time
import zmq
context = zmq.Context()
socket = context.socket(zmq.PUSH)
socket.connect("tcp://localhost:5555")


# f = open("buffer.dat","w")

rate = 100000 #Hz
nHits = 500000
times1 = np.linspace(1,nHits/2,nHits/2)
times2 = times1 + 0.001

# times.sort()

# channels = np.random.randint(50,size=nHits)

TopLayer = [0, 48, 1, 47, 2, 46, 3, 45, 4, 44, 5, 43, 6, 42, 7, 41]
BottomLayer = [9, 79, 10, 78, 11, 77, 12, 76, 13, 75, 14,
    74, 15, 73, 16, 72, 17, 71, 18, 70, 19, 69, 20, 68, 21, 67, 22, 66,
     23, 65, 24, 64, 25, 63, 26, 62, 27, 61, 28, 60, 29, 59, 30, 58,
      31, 57, 32, 56, 33, 55, 34, 54, 35, 53, 36, 52, 37, 51, 38, 50]


channels1 = np.random.choice(TopLayer,size=nHits/2)
channels2 = np.random.choice(BottomLayer,size=nHits/2)

times = np.column_stack((times1,times2)).flatten()
channels = np.column_stack((channels1,channels2)).flatten()

hit_stream = np.column_stack((channels,times))

print 'Starting Data stream at {} Hz'.format(rate)

sleepTime = 1.0/rate

# time.sleep(5)
time0 = time.time()
n=0
ctr=0
for ch,t in hit_stream:
    n+=1
    ctr+=1
    # f.write('{} {} \n'.format(ch,t))
    # f.flush()
    # print ch,t
    socket.send('{} {}'.format(ch,t))
    # socket.recv()
    t = (n*sleepTime) - (time.time() - time0)
    if t < 0:
        t = 0
    time.sleep(t)
    if ctr % rate ==0:
        print ctr
