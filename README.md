## README for Python Radiative Moller DAQ

### Charles Epstein
### October, 2017

Run this to install:

python setup.py build_ext --inplace

To clean, run:

python setup.py clean --all

If you're on the VME crate, or have python3 as default, running

python2 setup.py build_ext --inplace


When you first run, you might want to run

sudo python setup.py install

which will process dependencies.
